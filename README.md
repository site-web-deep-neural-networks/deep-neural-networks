# Site web - Deep neural networks

Ce site web a été réalisé sous Django.

Vous pouvez le découvrir à l'adresse suivante : www.deep-neural-networks.com

Ce projet est associé à une API présente dans le dépôt dénommé brain tumor detection api. Celle-ci a été crée à l'aide Django REST framework.

Cette API reçoit en entrée une image cérébrale présentant ou non une tumeur et propose en sortie une prédicion au moyen de différents modèles de classification de type CNN comme :

* VGG19;
* Resnet50;
* Inception-ResnetV2.


